/*
custom card for documentary
 */

import 'package:flutter/material.dart';
import 'package:molla_multimedia/Environment/helpers.dart';
import 'package:molla_multimedia/Environment/utilities.dart';
import 'package:molla_multimedia/Models/video.dart';

class DocumentaryCard extends StatelessWidget {
  // video to display
  final Video video;

  // aspect ration of the thumbnails
  final double aspectRatio = 16 / 9;

  const DocumentaryCard({Key key, @required this.video}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        shape: BeveledRectangleBorder(),
        margin: EdgeInsets.symmetric(horizontal: 0, vertical: 8),
        elevation: 0,
        child: Stack(
          //overflow: Overflow.visible,
          alignment: Alignment.center,
          children: <Widget>[
            AspectRatio(
              aspectRatio: aspectRatio,
              child: Container(
                child: FadeInImage.assetNetwork(
                  placeholder: "assets/images/news_default.png",
                  image: video.imageUrl,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Opacity(
                opacity: 1,
                child: Container(
                  color: Colors.black38,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        flex: 8,
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 8.0),
                          child: Text(
                            video.title,
                            maxLines: 2,
                            style: TextStyle(color: Colors.white, fontSize: 16),
                            softWrap: true,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 3,
                        child: Center(
                          child: ViewPiece.buildRawMaterialButton(
                              icon: Icons.play_arrow,
                              size: 50,
                              innerColor: Colors.white,
                              outerColor: Colors.red.shade600,
                              action: () =>
                                  playYoutubeVideo(context, video.id)),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
