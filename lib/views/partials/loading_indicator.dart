/*
  this library consists of the loading indicator partial view,
  used in asynchronous tasks
 */
import 'package:flutter/material.dart';
import 'package:molla_multimedia/Environment/all_translations.dart';

class LoadingIndicator extends StatelessWidget {
  // text to show under loading progress bar
  final String _loadingText;
  // check how many times refresh has been tried
  // for better feedback
  final int refreshCount;

  const LoadingIndicator(
      {Key key, @required String loadingText, int refreshCount = 0})
      : this._loadingText = loadingText,
        this.refreshCount = refreshCount,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            // change loading progress bar based on
            // refresh counts
            refreshCount == 0
                ? CircularProgressIndicator()
                : RefreshProgressIndicator(),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(translator.text(this._loadingText)),
            ),
          ]),
    );
  }
}
