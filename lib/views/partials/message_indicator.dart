/*
a view to show messages on the center of the screen
 */
import 'package:flutter/material.dart';
import 'package:molla_multimedia/Environment/all_translations.dart';

class MessageIndicator extends StatelessWidget {
  // message icon
  // message text
  final IconData icon;
  final String message;

  const MessageIndicator({Key key, this.icon, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Icon(
              icon,
              color: Theme.of(context).accentColor,
              size: 32,
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 8.0),
              child: Text(translator.text(this.message)))
        ]);
  }
}
