/*
a single audio resource view / widget
 */

import 'package:flutter/material.dart';
import 'package:fluttery_audio/fluttery_audio.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:molla_multimedia/Environment/helpers.dart';
import 'package:molla_multimedia/Models/audio.dart';
import 'package:molla_multimedia/Environment/globals.dart' as Globals;
import 'package:molla_multimedia/screens/home/radio_tab.dart';

class RadioCard extends StatefulWidget {
  final AudioPost audioPost;

  final Function(AudioPost audioPost) setCurrentlyPlayingAudio;

  final int playingAudioId;

  const RadioCard(
      {Key key,
      @required this.audioPost,
      @required this.setCurrentlyPlayingAudio,
      this.playingAudioId})
      : super(key: key);

  @override
  RadioCardState createState() {
    return new RadioCardState();
  }
}

class RadioCardState extends State<RadioCard> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 0, vertical: 4.0),
        shape: BeveledRectangleBorder(),
        color: widget.playingAudioId == widget.audioPost.id
            ? Theme.of(context).accentColor
            : Theme.of(context).backgroundColor,
        elevation: 0,
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  flex: 8,
                  child: ListTile(
                    onTap: () =>
                        widget.setCurrentlyPlayingAudio(widget.audioPost),
                    leading: widget.audioPost.imageUrl != null
                        ? FadeInImage.assetNetwork(
                            placeholder:
                                "assets/images/molla_media_alternate_logo.jpg",
                            image: widget.audioPost.imageUrl,
                            width: 100,
                          )
                        : Image.asset(
                            "assets/images/molla_media_alternate_logo.jpg",
                            width: 100,
                          ),
                    title: Text(widget.audioPost.title),
                    subtitle: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Expanded(
                          child: Text(constructReadableDate(
                              widget.audioPost.publishedDate)),
                        ),
                        Expanded(
                          child: IconButton(
                            icon: Icon(
                              FontAwesomeIcons.soundcloud,
                              color:
                                  widget.playingAudioId == widget.audioPost.id
                                      ? Colors.black54
                                      : Color(0xffff8800),
                            ),
                            onPressed: () {
                              launchURL(widget.audioPost.permalinkUrl);
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

//  Widget _buildIconBasedOnState() {
//    switch (widget.audioPlayer.state) {
//      case AudioPlayerState.loading:
//        return CircularProgressIndicator();
//      case AudioPlayerState.playing:
//        return IconButton(
//          icon: Icon(Icons.pause),
//          onPressed: () {
//            widget.audioPlayer.pause();
//          },
//        );
//        break;
//      case AudioPlayerState.paused:
//        return IconButton(
//          icon: Icon(Icons.play_arrow),
//          onPressed: () {
//            widget.audioPlayer.play();
//          },
//        );
//        break;
//
//      case AudioPlayerState.completed:
//        return IconButton(
//          icon: Icon(Icons.play_arrow),
//          onPressed: () {
//            widget.audioPlayer.loadMedia(Uri.parse(widget.audioPost.streamUrl +
//                "?client_id=" +
//                Globals.SOUNDCLOUD_CLIENT_ID));
//          },
//        );
//        break;
//      case AudioPlayerState.idle:
//      case AudioPlayerState.stopped:
//      default:
//        return IconButton(
//          icon: Icon(Icons.play_arrow),
//          onPressed: () {
//            widget.audioPlayer.loadMedia(Uri.parse(widget.audioPost.streamUrl +
//                "?client_id=" +
//                Globals.SOUNDCLOUD_CLIENT_ID));
//
//            widget.audioPlayer.play();
//          },
//        );
//        break;
//    }
//  }

//  @override
//  Widget build(BuildContext context) {
//    return Audio(
//      audioUrl: widget.audioPost.streamUrl +
//          "?client_id=" +
//          Globals.SOUNDCLOUD_CLIENT_ID,
//      playbackState: PlaybackState.paused,
//      buildMe: [
//        WatchableAudioProperties.audioPlayerState,
//      ],
//      playerBuilder: (BuildContext context, AudioPlayer player, Widget child) {
//        IconData icon = Icons.music_note;
//        if (player.state == AudioPlayerState.playing) {
//          icon = Icons.pause;
//        } else if (player.state == AudioPlayerState.paused) {
//          icon = Icons.play_arrow;
//        }
//
//        Function onPressed;
//        if (player.state == AudioPlayerState.playing) {
//          onPressed = player.pause;
//        } else if (player.state == AudioPlayerState.paused) {
//          onPressed = player.play;
//        }
//
//        return Container(
//          child: Card(
//            elevation: 0,
//            child: Column(
//              children: <Widget>[
//                Row(
//                  children: <Widget>[
//                    Expanded(
//                      flex: 8,
//                      child: ListTile(
//                        leading: Image.asset(
//                          "assets/images/molla_media_logo.png",
//                          width: 30,
//                        ),
//                        title: Text(widget.audioPost.title),
//                        subtitle: Text(constructReadableDate(
//                            widget.audioPost.publishedDate)),
//                      ),
//                    ),
//                    Expanded(
//                      flex: 2,
//                      child: IconButton(
//                        icon: new Icon(
//                          icon,
//                          size: 35.0,
//                        ),
//                        color: Colors.black,
//                        onPressed: onPressed,
//                      ),
//                    )
//                  ],
//                ),
//                AudioComponent(
//                  updateMe: [
//                    WatchableAudioProperties.audioLength,
//                    WatchableAudioProperties.audioPlayhead,
//                  ],
//                  playerBuilder:
//                      (BuildContext context, AudioPlayer player, Widget child) {
//                    return Container(
//                      child: new Slider(
//                        value: player.position == null ||
//                                player.audioLength == null
//                            ? 0.0
//                            : player.position.inMilliseconds.toDouble(),
//                        max: player.position == null ||
//                                player.audioLength == null
//                            ? 1.0
//                            : player.audioLength.inMilliseconds.toDouble(),
//                        onChanged: null,
//                      ),
//                    );
//                  },
//                ),
//              ],
//            ),
//          ),
//        );
//      },
//    );
//  }
}
