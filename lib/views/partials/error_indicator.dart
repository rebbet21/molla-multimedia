/*
error indicator library consists of a partial view widget
used to show friendly messages when error occurs
 */
import 'package:flutter/material.dart';
import 'package:molla_multimedia/Environment/all_translations.dart';

class ErrorIndicator extends StatelessWidget {
  final String _errorText;

  const ErrorIndicator({String errorText}) : this._errorText = errorText;

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Icon(
              Icons.error,
              color: Theme.of(context).accentColor,
              size: 32,
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 8.0),
              child: Text(translator.text(this._errorText)))
        ]);
  }
}
