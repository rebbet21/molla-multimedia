/*
  Top News constitutes of a single news view
  at the top of each tab
 */
import 'package:flutter/material.dart';
import 'package:molla_multimedia/Environment/globals.dart' as Globals;
import 'package:molla_multimedia/Environment/helpers.dart';
import 'package:molla_multimedia/Environment/utilities.dart';
import 'package:molla_multimedia/Models/Post.dart';
import 'package:molla_multimedia/Models/news.dart';
import 'package:molla_multimedia/Models/video.dart';
import 'package:molla_multimedia/screens/home/news_details.dart';

class TopPost extends StatelessWidget {
  final Post post;

  final String imageHeroTag;

  const TopPost({@required this.post, @required this.imageHeroTag});

  @override
  Widget build(BuildContext context) {
    // get device width
    final double width = MediaQuery.of(context).size.width;
    // set the container's width as a ration of the device width
    final double containerWidth = width * .9;
    // maintain standard aspect ration for the height
    final double aspectRation = 16 / 9;

    return Container(
      width: containerWidth,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          // show play icon if its a video

          // the image or thumbnail background
          Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  onTap: () {
                    // check the instance of post whether
                    // it is news or video
                    if (post is News) {
                      News news = post;
                      // navigate to details
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => NewsDetails(
                                    news: news,
                                    imageHeroTag: imageHeroTag,
                                  )));
                    }
                  },
                  child: Hero(
                    tag: imageHeroTag,
                    child: AspectRatio(
                      aspectRatio: aspectRation,
                      child: FadeInImage.assetNetwork(
                          fit: BoxFit.cover,
                          placeholder: 'assets/images/news_default.png',
                          image: post.imageUrl ?? ''),
                    ),
                  ),
                ),
              )),

          post is Video
              ? Padding(
                  padding: EdgeInsets.only(bottom: 50),
                  child: ViewPiece.buildRawMaterialButton(
                      innerColor: Colors.white,
                      outerColor: Colors.red.shade600,
                      action: () =>
                          playYoutubeVideo(context, (post as Video).id),
                      shape: StadiumBorder(),
                      size: 50))
              : Container(width: 0, height: 0),

          // title and other few details about the post
          // use different layout for news
          // and video
          (post is News) ? _buildNewsCard(context) : _buildVideoCard(context)
        ],
      ),
    );
  }

  Positioned _buildVideoCard(BuildContext context) {
    return Positioned(
      bottom: 5,
      left: 0,
      right: 0,
      child: Card(
        elevation: 0,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Flexible(
              flex: 6,
              child: InkWell(
                onTap: () {
                  // play video
                  playYoutubeVideo(context, (post as Video).id);
                },
                child: Container(
                  child: ListTile(
                    title: Text(
                      post.title,
                      style:
                          TextStyle(fontWeight: FontWeight.w100, fontSize: 16),
                    ),
                    dense: true,
                    subtitle: Container(
                      child: Text(
                        constructReadableDate(post.publishedDate),
                      ),
                    ),
                    leading: Image.asset(
                      "assets/images/molla_media_logo.png",
                      width: 40,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Positioned _buildNewsCard(BuildContext context) {
    return Positioned(
      bottom: 10,
      right: 10,
      left: 10,
      child: Material(
        color: Colors.transparent,
        child: Card(
          color: Globals.styles['topCardBackgroundColor'],
          child: InkWell(
            onTap: () {
              // navigate to details
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          NewsDetails(news: post, imageHeroTag: imageHeroTag)));
            },
            child: Column(
              children: <Widget>[
                Container(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      post.title,
                      style: Globals.styles['title'],
                    )),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 8),
                  child: Divider(
                    height: 1,
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    post is News
                        ? Container(
                            padding: EdgeInsets.symmetric(horizontal: 8.0),
                            child: Chip(
                                label: Text(
                              resolveCategoryName((post as News).categoryId),
                            )),
                          )
                        : Container(
                            width: 0,
                            height: 0,
                          ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      child: Text(
                        constructReadableDate(post.publishedDate),
                        style: Globals.styles['date'],
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
