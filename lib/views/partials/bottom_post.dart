import 'package:flutter/material.dart';
import 'package:molla_multimedia/Environment/helpers.dart';
import 'package:molla_multimedia/Environment/utilities.dart';
import 'package:molla_multimedia/Models/Post.dart';
import 'package:molla_multimedia/Models/news.dart';
import 'package:molla_multimedia/Models/video.dart';
import 'package:molla_multimedia/screens/home/news_details.dart';

class BottomPost extends StatelessWidget {
  final Post post;

  final String heroTag;

  const BottomPost({this.post, this.heroTag});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        child: InkWell(
          onTap: () {
            // check the instance of post whether
            // it is news or video
            if (post is News) {
              News news = post;
              // navigate to details
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          NewsDetails(news: news, imageHeroTag: heroTag)));
            } else if (post is Video) {
              // play the video
              //playYoutubeVideo(context, (post as Video).id);
            }
          },
          child: Card(
            elevation: 0,
            color: Color(0xFFFFFFFF),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        post is News
                            ? _buildNewsDetails(context)
                            : _buildVideoDetails(context)
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Stack(alignment: Alignment.center, children: <Widget>[
                    Container(
                      child: Hero(
                        tag: heroTag,
                        child: AspectRatio(
                          aspectRatio: 4 / 3,
                          child: FadeInImage.assetNetwork(
                            placeholder: 'assets/images/news_default.png',
                            image: post.imageUrl ?? '',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    post is News
                        ? Positioned(
                            right: 0,
                            bottom: 0,
                            child: Chip(
                                label: Text(resolveCategoryName(
                                    (post as News).categoryId))),
                          )
                        : Positioned(
                            child: ViewPiece.buildRawMaterialButton(
                                size: 40,
                                icon: Icons.play_arrow,
                                outerColor: Colors.red.shade600,
                                innerColor: Colors.white,
                                action: () => playYoutubeVideo(
                                    context, (post as Video).id)))
                  ]),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildVideoDetails(BuildContext context) {
    return Container(
      child: ListTile(
        title: Text(
          post.title,
          style: TextStyle(fontWeight: FontWeight.w100, fontSize: 16),
        ),
        dense: true,
        subtitle: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                constructReadableDate(post.publishedDate),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildNewsDetails(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          child: Text(post.title),
        ),
        Container(
          padding: EdgeInsets.only(top: 10),
          child: Text((post as News).author,
              style: TextStyle(color: Theme.of(context).accentColor)),
        ),
        Container(
            child: Text(
          constructReadableDate(post.publishedDate),
          style: Theme.of(context).textTheme.subtitle,
        ))
      ],
    );
  }
}
