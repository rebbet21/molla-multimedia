/*
list builder for top news
 */
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:molla_multimedia/Environment/globals.dart' as Globals;
import 'package:molla_multimedia/Models/Post.dart';
import 'package:molla_multimedia/views/partials/top_post.dart';

class TopPostsListBuilder extends StatelessWidget {
  const TopPostsListBuilder({@required this.allPosts});

  final List<Post> allPosts;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      key: new PageStorageKey(Globals.topNewsUniqueNameKey),
      itemBuilder: (context, index) {
        // create a news instance from json data
        // and build a top news card

        return TopPost(
          post: allPosts[index],
          // create a unique hero tag
          imageHeroTag: Globals.topImageHeroTagPrefix +
              index.toString() +
              "_" +
              allPosts[index].title +
              "_" +
              allPosts[index].publishedDate.toString() +
              "_" +
              Random.secure().nextDouble().toString(),
        );
      },
      itemCount: allPosts.length,
      scrollDirection: Axis.horizontal,
    );
  }
}
