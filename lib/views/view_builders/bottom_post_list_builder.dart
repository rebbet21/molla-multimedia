// bottom news list builder

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:molla_multimedia/Environment/globals.dart' as Globals;
import 'package:molla_multimedia/Models/Post.dart';
import 'package:molla_multimedia/views/partials/bottom_post.dart';

class BottomPostListBuilder extends StatelessWidget {
  BottomPostListBuilder({@required this.allPosts});

  // list of news originally shown
  final List<Post> allPosts;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      key: new PageStorageKey(Globals.bottomNewsUniqueNameKey),
      itemBuilder: (context, index) {
        return BottomPost(
            post: allPosts[index],
            heroTag: Globals.bottomImageHeroTagPrefix +
                index.toString() +
                "_" +
                allPosts[index].title +
                "_" +
                allPosts[index].publishedDate.toString() +
                "_" +
                Random.secure().nextDouble().toString());
      },
      shrinkWrap: true,
      itemCount: allPosts.length,
      physics: NeverScrollableScrollPhysics(),
    );
    ;
  }
}
