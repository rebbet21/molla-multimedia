import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:molla_multimedia/Environment/all_translations.dart';
import 'package:molla_multimedia/Environment/globals.dart' as Globals;
import 'package:molla_multimedia/Environment/helpers.dart';
import 'package:molla_multimedia/screens/home/home_container.dart';
import 'package:molla_multimedia/screens/settings.dart';
import 'package:molla_multimedia/screens/with_molla/with_molla_container.dart';

void main() async {
  // Initializes the translation module
  await translator.init();
  // start the application
  runApp(MollaMediaApp());
}

class MollaMediaApp extends StatefulWidget {
  @override
  MollaMediaAppState createState() {
    return new MollaMediaAppState();
  }
}

class MollaMediaAppState extends State<MollaMediaApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: translator.text('app title'),
      theme: Globals.appTheme,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      //supported locales by the application
      supportedLocales: translator.supportedLocales(),
      home: HomePage(),
    );
  }

  @override
  void initState() {
    super.initState();
    translator.onLocaleChangedCallback = _onLocaleChanged;
  }

  void _onLocaleChanged() async {
    // do something when language changes
    print('language changed to ' + translator.currentLanguage);
  }
}

class HomePage extends StatefulWidget {
  final int mainPageIndex;

  const HomePage({Key key, this.mainPageIndex = Globals.HOME_PAGE_INDEX})
      : super(key: key);

  @override
  _HomePageState createState() => _HomePageState(mainPageIndex);
}

class _HomePageState extends State<HomePage> {
  _HomePageState(this.bottomPageIndex);

  // get currently preferred language
  // default is amharic
  final String language = translator.currentLanguage;

  // home page
  final HomeContainer homeContainer = HomeContainer();
  // with molla page
  final WithMollaTabContainer _withMollaTabContainer = WithMollaTabContainer();
  // settings page
  final SettingsTab _settingsContainer = SettingsTab();

  // bottom navigation bar index
  // default is home
  int bottomPageIndex;

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, () {
      setOverlayColor(Colors.grey, Theme.of(context).primaryColorDark);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: choosePage(),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              title: Text(translator.text('home'),
                  style: Globals.styles['bottomNavigationTextStyle']),
              icon: Icon(Icons.home)),
          BottomNavigationBarItem(
              title: Text(translator.text('with molla'),
                  style: Globals.styles['bottomNavigationTextStyle']),
              icon: Icon(Icons.people)),
          BottomNavigationBarItem(
              title: Text(translator.text('settings'),
                  style: Globals.styles['bottomNavigationTextStyle']),
              icon: Icon(Icons.settings)),
        ],
        currentIndex: this.bottomPageIndex,
        onTap: (index) {
          setState(() {
            this.bottomPageIndex = index;
          });
        },
      ),
    );
  }

  // choose which bottom page container to show
  Widget choosePage() {
    switch (bottomPageIndex) {
      case Globals.HOME_PAGE_INDEX:
        return homeContainer;
        break;
      case Globals.WITH_MOLLA_PAGE_INDEX:
        return _withMollaTabContainer;
        break;
      case Globals.SETTINGS_PAGE_INDEX:
        return _settingsContainer;
        break;
      default:
        return homeContainer;
    }
  }

  @override
  bool get wantKeepAlive => true;
}
