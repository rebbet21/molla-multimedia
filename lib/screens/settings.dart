/*

  This is the screen shown when the bottom navigation bar 'settings' is selected
 */
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:molla_multimedia/Environment/all_translations.dart';
import 'package:molla_multimedia/Environment/globals.dart' as Globals;
import 'package:molla_multimedia/Environment/helpers.dart';
import 'package:molla_multimedia/main.dart';

class SettingsTab extends StatefulWidget {
  @override
  _SettingsTabState createState() => _SettingsTabState();
}

class _SettingsTabState extends State<SettingsTab> {
  List _languages = translator.supportedLanguages;

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentLang;

  @override
  Widget build(BuildContext context) {
    return NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 250.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                title: Text(
                  translator.text("app title"),
                  style: TextStyle(color: Colors.black54),
                ),
                centerTitle: true,
                background: Image.asset(
                  "assets/images/molla_multimedia_map.png",
                  fit: BoxFit.cover,
                ),
              ),
            )
          ];
        },
        body: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              margin: EdgeInsets.symmetric(vertical: 4.0),
              color: Colors.grey.shade100,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(flex: 1, child: Icon(Icons.location_on)),
                      Expanded(
                        flex: 2,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 8.0),
                                child: Text(translator.text("address text")),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),

//            Padding(
//              padding: EdgeInsets.symmetric(horizontal: 8.0),
//              child: Divider(
//                color: Theme.of(context).primaryColor,
//              ),
//            ),
            Container(
              alignment: Alignment.center,
              width: 200,
              child: ListTile(
//                  leading: Icon(Icons.settings),
                title: Text(translator.text("settings")),
                subtitle: Text(translator.text("change settings")),
              ),
            ),
//            Divider(),
            ListTile(
              leading: Icon(Icons.language),
              subtitle: Text(translator.text('language')),
              title: Container(
                  child: new DropdownButton(
                hint: Text(translator.text("language")),
                elevation: 0,
                value: _currentLang,
                items: _dropDownMenuItems,
                onChanged: changedDropDownItem,
              )),
            ),
//            Divider(),
            Container(
              alignment: Alignment.center,
              width: 200,
              child: ListTile(
//                  leading: Icon(Icons.settings),
                title: Text(translator.text("connect")),
                subtitle: Text(translator.text("with many choices")),
              ),
            ),
//            Divider(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: ListTile(
                        title: Text(translator.text("website")),
                        leading: Icon(FontAwesomeIcons.globe),
                        onTap: () {
                          launchURL(Globals.BASE_URI);
                        },
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: ListTile(
                        title: Text(translator.text("facebook")),
                        leading: Icon(FontAwesomeIcons.facebook),
                        onTap: () {
                          launchURL(Globals.FACEBOOK_ACCOUNT_LINK);
                        },
                      ),
                    )
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: ListTile(
                        title: Text(translator.text("youtube")),
                        leading: Icon(FontAwesomeIcons.youtube),
                        onTap: () {
                          launchURL(Globals.YOUTUBE_CHANNEL_LINK);
                        },
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: ListTile(
                        title: Text(translator.text("soundcloud")),
                        leading: Icon(FontAwesomeIcons.soundcloud),
                        onTap: () {
                          launchURL(Globals.SOUNDCLOUD_ACCOUNT_LINK);
                        },
                      ),
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 8.0),
                  margin: EdgeInsets.symmetric(vertical: 4.0),
                  color: Colors.grey.shade100,
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: ListTile(
                              title: Text(
                                translator.text("app title"),
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.w500),
                              ),
                              subtitle:
                                  Text(translator.text("voice of reason")),
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 8.0),
                                    child: Text(
                                      translator.text("about us"),
                                      textAlign: TextAlign.justify,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  color: Theme.of(context).backgroundColor,
                  alignment: Alignment.center,
                  child: Image.asset(
                    "assets/images/molla_media_alternate_logo.jpg",
                    width: 200,
                  ),
                ),
              ],
            )
          ],
        ));
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String language in _languages) {
      items.add(new DropdownMenuItem(
          value: language, child: new Text(resolveLanguageName(language))));
    }
    return items;
  }

  void changedDropDownItem(String selectedLang) async {
    await translator.setNewLanguage(selectedLang);
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return HomePage(
        mainPageIndex: Globals.SETTINGS_PAGE_INDEX,
      );
    }));
  }

  @override
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _currentLang = translator.currentLanguage;

    super.initState();
  }
}
