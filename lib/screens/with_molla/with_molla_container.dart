import 'package:flutter/material.dart';
import 'package:molla_multimedia/Environment/all_translations.dart';
import 'package:molla_multimedia/Environment/globals.dart' as Globals;
import 'package:molla_multimedia/Environment/utilities.dart';
import 'package:molla_multimedia/screens/home/post_tab.dart';

class WithMollaTabContainer extends StatefulWidget {
  final List tabs = ['what molla said', 'discuss with molla'];

  @override
  WithMollaTabContainerState createState() => WithMollaTabContainerState();
}

class WithMollaTabContainerState extends State<WithMollaTabContainer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length: 2,
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                expandedHeight: 200.0,
                floating: false,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                    centerTitle: true,
                    title: Text(translator.text("with ato molla"),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                        )),
                    background: Image.asset(
                      "assets/images/ato_molla.jpg",
                      fit: BoxFit.cover,
                    )),
              ),
              SliverPersistentHeader(
                floating: false,
                delegate: SliverAppBarDelegate(
                  TabBar(
                      unselectedLabelColor:
                          Globals.styles['unselectedTabLabelColor'],
                      labelColor: Globals.styles['selectedTabLabelColor'],
                      unselectedLabelStyle:
                          Globals.styles['unselectedTopTabStyle'],
                      labelStyle: Globals.styles['selectedTopTabStyle'],
                      tabs: widget.tabs.map((tabName) {
                        return Tab(
                          text: translator.text(tabName),
                        );
                      }).toList()),
                ),
                pinned: true,
              ),
            ];
          },
          body: TabBarView(children: <Widget>[
            //first tab is molla min ale
            PostTab(
                selectedCategoryName: 'what molla said',
                tabType: Globals.VIDEO_TYPE,
                playlistId: Globals.YOUTUBE_MOLLA_MIN_ALE_PLAYLIST_ID),
            // second tab is ke molla ga wiyiyit
            PostTab(
              selectedCategoryName: 'discuss with molla',
              tabType: Globals.VIDEO_TYPE,
              playlistId: Globals.YOUTUBE_KE_MOLLA_GA_WIYIYIT_PLAYLIST_ID,
            ),
          ]),
        ),
      ),
    );
  }
}
