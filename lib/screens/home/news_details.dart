/*
this screen provides details of a news post
 */

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:molla_multimedia/Environment/all_translations.dart';
import 'package:molla_multimedia/Environment/globals.dart' as Globals;
import 'package:molla_multimedia/Environment/helpers.dart';
import 'package:molla_multimedia/Models/news.dart';

import 'package:flutter_html/flutter_html.dart';

class NewsDetails extends StatefulWidget {
  // news to show details for
  final News news;
  // unique hero tag id
  final String imageHeroTag;

  const NewsDetails({Key key, @required this.news, @required this.imageHeroTag})
      : super(key: key);

  @override
  _NewsDetailsState createState() => _NewsDetailsState();
}

class _NewsDetailsState extends State<NewsDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            //backgroundColor: Theme.of(context).backgroundColor,
            title: widget.news.title.length >
                    Globals.TITLE_CHAR_LENGTH_LIMIT_FOR_POSITIONING
                ? buildTitleText()
                : null,
            pinned: true,
            //title: Container(child: Text(translator.text(widget.news.title))),
            floating: false,
            flexibleSpace: FlexibleSpaceBar(
              title: widget.news.title.length <
                      Globals.TITLE_CHAR_LENGTH_LIMIT_FOR_POSITIONING
                  ? buildTitleText()
                  : null,
              centerTitle: false,
              collapseMode: CollapseMode.parallax,
              background: Hero(
                tag: widget.imageHeroTag,
                child: widget.news.imageUrl != null
                    ? Image.network(
                        widget.news.imageUrl,
                        fit: BoxFit.cover,
//            height: 200,
                      )
                    : Image.asset(
                        "assets/images/news_default.png",
                        fit: BoxFit.cover,
                      ),
              ),
            ),

            expandedHeight: 200,

//          title: Image.asset(
//            "assets/images/news_default.png",
//            fit: BoxFit.contain,
//            height: 200,
//          ),
          ),
          SliverList(
              delegate: SliverChildListDelegate(<Widget>[
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(
                  top: 10.0, bottom: 10.0, left: 20.0, right: 20.0),
              child: Text(
                widget.news.title,
                style: Globals.styles['title'],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    FontAwesomeIcons.shareAltSquare,
                    color: Colors.grey.shade600,
                  ),
                  SizedBox(
                    width: 8.0,
                  ),
                  Text(translator.text("share")),
                ],
              ),
            ),
            Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      FontAwesomeIcons.facebookF,
                      color: Theme.of(context).accentColor,
                      size: 30,
                    ),
                    tooltip: translator.text("share with facebook"),
                    onPressed: () => launchURL(
                        Globals.FACEBOOK_SHARER_LINK + widget.news.link),
                  ),
                  IconButton(
                    icon: Icon(
                      FontAwesomeIcons.twitter,
                      color: Theme.of(context).accentColor,
                      size: 30,
                    ),
                    tooltip: translator.text("share with twitter"),
                    onPressed: () => launchURL(
                        Globals.TWITTER_SHARER_LINK + widget.news.link),
                  ),
                  IconButton(
                    icon: Icon(
                      FontAwesomeIcons.telegramPlane,
                      color: Theme.of(context).accentColor,
                      size: 30,
                    ),
                    tooltip: translator.text("share with telegram"),
                    onPressed: () => launchURL(
                        Globals.TELEGRAM_SHARER_LINK + widget.news.link),
                  ),
                  IconButton(
                    icon: Icon(
                      FontAwesomeIcons.pinterestP,
                      color: Theme.of(context).accentColor,
                      size: 30,
                    ),
                    tooltip: translator.text("share with pinterest"),
                    onPressed: () => launchURL(
                        Globals.PINTEREST_SHARER_LINK + widget.news.link),
                  ),
                ],
              ),
              color: Globals.styles['topCardBackgroundColor'],
            ),
            Divider(
              indent: 8.0,
            ),
            Container(
              alignment: Alignment.centerRight,
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Icon(
                    FontAwesomeIcons.marker,
                    size: 14,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text(
                    translator.text("by") + " " + widget.news.author,
                  ),
                ],
              ),
            ),
            Divider(
              indent: 8.0,
            ),
            Container(
              alignment: Alignment.centerRight,
              child: FittedBox(
                fit: BoxFit.fitWidth,
                child: Divider(
                  color: Theme.of(context).accentColor,
                  indent: 8.0,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: Html(
                data: widget.news.description,
                renderNewlines: true,
              ),
            ),
            Container(
              color: Globals.styles['topCardBackgroundColor'],
              padding: EdgeInsets.only(left: 20.0),
              child: Center(
                child: ListTile(
                  leading: Image.asset("assets/images/molla_media_logo.png",
                      width: 40),
                  onTap: () => launchURL(widget.news.link),
                  title: Text(translator.text('open in browser')),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Column(),
            )
          ]))
        ],
      ),
    );
  }

  // title text builder
  Text buildTitleText() {
    return Text(
      translator.text(widget.news.title),
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(fontSize: 16),
    );
  }
}
