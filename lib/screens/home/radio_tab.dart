import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:fluttery_audio/fluttery_audio.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:molla_multimedia/Environment/all_translations.dart';
import 'package:molla_multimedia/Environment/globals.dart' as Globals;
import 'package:molla_multimedia/Environment/helpers.dart';
import 'package:molla_multimedia/Environment/utilities.dart';
import 'package:molla_multimedia/Models/audio.dart';
import 'package:molla_multimedia/views/partials/error_indicator.dart';
import 'package:molla_multimedia/views/partials/loading_indicator.dart';
import 'package:molla_multimedia/views/partials/message_indicator.dart';
import 'package:molla_multimedia/views/partials/radio_card.dart';

class RadioTab extends StatefulWidget {
  ScrollController _scrollController = ScrollController();

  RadioTab({Key key}) : super(key: key);

  @override
  RadioTabState createState() => new RadioTabState();
}

class RadioTabState extends State<RadioTab>
    with AutomaticKeepAliveClientMixin<RadioTab> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  AsyncMemoizer _memoizer = AsyncMemoizer();

  int refreshCounter = 0;

  int nextPage = 0;

  List<AudioPost> moreAudiosList = [];

  bool isPerformingMorePostsRequest = false;

  // currently playing audio
  AudioPost playingAudio;

  // audio playing capabilities
  final AudioPlayer audioPlayer = FlutteryAudio.audioPlayer();

  Duration audioLength;
  Duration position;

  AudioPlayerState _audioPlayerStatetate;

  @override
  void initState() {
    // fetch more data when scroll is
    // exhausted
    widget._scrollController.addListener(() {
      if (widget._scrollController.position.pixels ==
          widget._scrollController.position.maxScrollExtent) {
        _loadMoreAudios();
      }
    });
    //
    audioPlayer.addListener(onStateChanged: (state) {
      setState(() {
        _audioPlayerStatetate = state;
      });
    }, onAudioLengthChanged: (Duration audioLength) {
      setState(() {
        this.audioLength = audioLength;
      });
    }, onPlayerPlaybackUpdate: (Duration position) {
      setState(() {
        this.position = position;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: () {
        return Future.delayed(Duration(milliseconds: 1)).then((result) {
          // reset some states
          setState(() {
            audioPlayer.stop();
            //audioPlayer.dispose();

            _memoizer = AsyncMemoizer();

            refreshCounter = 0;

            nextPage = 0;

            moreAudiosList = [];

            isPerformingMorePostsRequest = false;

            playingAudio = null;
          });
        });
      },
      child: FutureBuilder(
          future: _fetchInitialAudiosList(),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
                return LoadingIndicator(
                  loadingText: "connecting",
                  refreshCount: refreshCounter,
                );
                break;
              case ConnectionState.active:
                return LoadingIndicator(
                  loadingText: "active",
                  refreshCount: refreshCounter,
                );
                break;
              case ConnectionState.done:
                if (snapshot.hasError) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ErrorIndicator(errorText: "connection error text"),
                      _buildTryAgainButton(context)
                    ],
                  );
                } else if (snapshot.hasData) {
                  // parse the data from the future object
                  // and create a list of posts using
                  // the data from the snapshot
                  // add list of new posts
                  // to the all news list

                  List<AudioPost> allAudiosList = getPostsList(
                          jsonData: snapshot.data, type: Globals.AUDIO_TYPE)
                      .cast<AudioPost>();

                  allAudiosList.addAll(moreAudiosList);
                  // remove duplicates and sort by creation date
                  allAudiosList = allAudiosList.toSet().toList(growable: false);
                  allAudiosList.sort((AudioPost first, AudioPost second) =>
                      second.publishedDate
                          .difference(first.publishedDate)
                          .inMilliseconds);
                  // show the data
                  return _buildScreenListView(allAudiosList);
                } else {
                  // check if no post is found in the response
                  // then show feedback message

                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      MessageIndicator(
                          icon: FontAwesomeIcons.hourglass, message: "no data"),
                      FlatButton(
                          child: Text(translator.text("try again")),
                          color: Globals.styles['flatButtonColor'],
                          shape: StadiumBorder(),
                          onPressed: () {
                            setState(() {
                              // refresh tab with a new memoizer
                              // to reload the future in
                              // the future builder
                              _memoizer = AsyncMemoizer();
                            });
                          })
                    ],
                  );
                }
                break;
            }
          }),
    );
  }

  @override
  void dispose() {
    super.dispose();

    audioPlayer.stop();
    audioPlayer.dispose();
  }

  Widget _buildIconBasedOnState() {
    switch (_audioPlayerStatetate) {
      case AudioPlayerState.loading:
        return CircularProgressIndicator();
      case AudioPlayerState.playing:
        return IconButton(
          icon: Icon(Icons.pause),
          onPressed: () {
            audioPlayer.pause();
          },
          iconSize: 38,
        );
        break;
      case AudioPlayerState.paused:
        return IconButton(
          icon: Icon(Icons.play_arrow),
          onPressed: () {
            audioPlayer.play();
          },
          iconSize: 38,
        );
        break;

      case AudioPlayerState.idle:
      case AudioPlayerState.stopped:
      case AudioPlayerState.completed:
      default:
        return IconButton(
          icon: Icon(Icons.play_arrow),
          onPressed: () {
            audioPlayer.play();
          },
          iconSize: 38,
        );
        break;
    }
  }

  setCurrentlyPlayingAudio(AudioPost audioPost) {
    setState(() {
      playingAudio = audioPost;
    });

    audioPlayer.loadMedia(Uri.parse(
        audioPost.streamUrl + "?client_id=" + Globals.SOUNDCLOUD_CLIENT_ID));

    audioPlayer.play();
  }

  // retrieve api uri to request for data
  String _getUri() {
    return Globals.SOUNDCLOUD_API_URI;
  }

  // construct request parameters for the request
  Map<String, dynamic> _constructParameters() {
    return {
      'client_id': Globals.SOUNDCLOUD_CLIENT_ID,
      'limit': Globals.SOUNDCLOUD_MAX_RESULT_LIMIT.toString(),
      'offset': (Globals.SOUNDCLOUD_MAX_RESULT_LIMIT * nextPage).toString(),
      'order': 'created_at'
    };
  }

  // load initial posts data
  _fetchInitialAudiosList() {
    // make request
    return this._memoizer.runOnce(() async {
      return await Parachute.fetchRemoteData(_getUri(),
          parameters: _constructParameters());
    });
  }

  // load more audios from api
  Future _loadMoreAudios() async {
    // fetch new posts and update the state
    // if no other request is ongoing
    if (!isPerformingMorePostsRequest) {
      // set is performing request to true
      setState(() {
        isPerformingMorePostsRequest = true;
        nextPage++;
      });

      //fetch more posts
      dynamic newAudioPosts = await Parachute.fetchRemoteData(_getUri(),
          parameters: _constructParameters());

      // animate when no data is available
      if (newAudioPosts == null) {
        double edge = 50.0;
        double offsetFromBottom =
            widget._scrollController.position.maxScrollExtent -
                widget._scrollController.position.pixels;
        if (offsetFromBottom < edge) {
          widget._scrollController.animateTo(
              widget._scrollController.offset -
                  (edge - offsetFromBottom) -
                  (16),
              duration: new Duration(milliseconds: 500),
              curve: Curves.easeOut);
        }
      }

      // update the state
      setState(() {
        // try to parse the json data and convert to Post
        // object before adding to more news
        try {
          // thanks to shitty soundcloud pagination api,
          // we have to check if the item
          // already exists in prior
          // responses before adding
          // it to our list

          moreAudiosList.addAll(
              getPostsList(jsonData: newAudioPosts, type: Globals.AUDIO_TYPE)
                  .cast<AudioPost>());
          // remove duplicates
          moreAudiosList = moreAudiosList.toSet().toList();
        } catch (e) {
          Scaffold.of(context).hideCurrentSnackBar();
          Scaffold.of(context).showSnackBar(ViewPiece.actionableSnackBar(
              leftText: "connection error text",
              rightText: "visit soundcloud",
              rightAction: () => launchURL(Globals.SOUNDCLOUD_ACCOUNT_LINK)));
        }

        isPerformingMorePostsRequest = false;
      });
    }
  }

  // try again indicator
  _buildTryAgainButton(BuildContext context) {
    return Container(
      child: FlatButton(
          color: Globals.styles['flatButtonColor'],
          shape: StadiumBorder(),
          onPressed: () {
            setState(() {
              // increment refresh counter
              refreshCounter++;
            });
            // show a message when refresh counter reaches
            // certain limit
            if (refreshCounter >=
                Globals
                    .NUMBER_OF_REFRESH_LIMIT_TO_SHOW_VISIT_WEBSITE_SUGGESTION) {
              // show visit website suggestion
              Scaffold.of(context).hideCurrentSnackBar();
              Scaffold.of(context).showSnackBar(ViewPiece.actionableSnackBar(
                  leftText: "connection error text",
                  rightText: "visit soundcloud",
                  rightAction: () =>
                      launchURL(Globals.SOUNDCLOUD_ACCOUNT_LINK)));
            }
          },
          child: Text(translator.text("try again"))),
    );
  }

  Widget _buildScreenListView(List<AudioPost> allAudioPosts) {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Center(child: _buildIconBasedOnState()),
                ),
                Expanded(
                  flex: 9,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: 8.0),
                        child: Text(
                          playingAudio != null
                              ? playingAudio.title
                              : translator.text("no audio playing"),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Slider(
                          inactiveColor: Color(0x11ff8800),
                          activeColor: Color(0xffff8800),
                          value: position == null ||
                                  audioLength == null ||
                                  position.inMilliseconds >=
                                      audioLength.inMilliseconds
                              ? 0.0
                              : position.inMilliseconds.toDouble(),
                          max: audioLength == null
                              ? 1.0
                              : audioLength.inMilliseconds.toDouble(),
                          onChanged: (double newValue) {
                            audioPlayer
                                .seek(Duration(milliseconds: newValue.toInt()));
                          }),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          flex: 6,
          child: ListView.builder(
            itemBuilder: (context, index) {
              // fetching more indicator
              if (index == allAudioPosts.length) {
                return isPerformingMorePostsRequest
                    ? Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: LoadingIndicator(
                          loadingText: "fetching more",
                        ),
                      )
                    : Container(
                        margin: EdgeInsets.symmetric(vertical: 8.0),
                        child: IconButton(
                          icon: Icon(
                            Icons.arrow_downward,
                          ),
                          onPressed: () async {
                            _loadMoreAudios();
                          },
                        ));
              }
              return RadioCard(
                setCurrentlyPlayingAudio: setCurrentlyPlayingAudio,
                playingAudioId: playingAudio != null ? playingAudio.id : 0,
                audioPost: allAudioPosts[index],
              );
            },
            itemCount: allAudioPosts.length + 1,
            controller: widget._scrollController,
          ),
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
