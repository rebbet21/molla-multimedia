import 'dart:async';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:molla_multimedia/Environment/all_translations.dart';
import 'package:molla_multimedia/Environment/globals.dart' as Globals;
import 'package:molla_multimedia/Environment/helpers.dart';
import 'package:molla_multimedia/Environment/utilities.dart';
import 'package:molla_multimedia/Models/video.dart';
import 'package:molla_multimedia/views/partials/documentary_card.dart';
import 'package:molla_multimedia/views/partials/error_indicator.dart';
import 'package:molla_multimedia/views/partials/loading_indicator.dart';
import 'package:molla_multimedia/views/partials/message_indicator.dart';

class DocumentaryTab extends StatefulWidget {
  DocumentaryTab({Key key, this.playlistId}) : super(key: key);

  @override
  _DocumentaryTabState createState() => _DocumentaryTabState();

  // controller
  final ScrollController _scrollController = ScrollController();

  // playlist to fetch videos from
  final String playlistId;
}

class _DocumentaryTabState extends State<DocumentaryTab>
    with AutomaticKeepAliveClientMixin<DocumentaryTab> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  // for videos, store the next page token
  // from youtube api
  String nextVideoPageToken = '';
  // controller for the stream
  StreamController _streamController = StreamController();

  // loaded videos list
  List<Video> videosList = [];

  // if request is under-going
  bool isRequestUnderGoing = false;
  // count refreshes for feedback purposes
  int refreshCounter = 0;

  @override
  void initState() {
    super.initState();
    // add event to the scrollable listview to
    // fetch more data when scroll is
    // exhausted
    widget._scrollController.addListener(() {
      if (widget._scrollController.position.pixels ==
          widget._scrollController.position.maxScrollExtent) {
        _loadVideos();
      }
    });

    // get videos initially
    _loadVideos();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () {
        return Future.delayed(Duration(milliseconds: 1)).then((result) {
          //reset some state
          setState(() {
            nextVideoPageToken = '';

            _streamController = StreamController();

            videosList = [];

            isRequestUnderGoing = false;
            refreshCounter = 0;
          });

          _loadVideos();
        });
      },
      key: _refreshIndicatorKey,
      child: StreamBuilder(
        stream: _streamController.stream,
        builder: (builder, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              return LoadingIndicator(
                loadingText: "connecting",
                refreshCount: refreshCounter,
              );
              break;

            // done means the stream is closed
            case ConnectionState.done:
              return _buildVideosListView();
              break;
            // active connection state means
            // the stream is receiving
            // data
            case ConnectionState.active:
              if (snapshot.hasError) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    ErrorIndicator(errorText: "connection error text"),
                    _buildTryAgainButton(context)
                  ],
                );
              } else if (snapshot.hasData) {
                // show loading if a request under-going
                if (isRequestUnderGoing && videosList.isEmpty) {
                  return LoadingIndicator(
                    loadingText: "active",
                  );
                }
                // check if data is empty
                if (snapshot.data.isEmpty) {
                  return _buildNoDataFeedback();
                }

                // do not add the stream data when a request is undergoing,
                /*
                  when setState is called (to show loading indicator),
                  it refreshes the build method, and
                  the stream contains the last data added to it.
                  this creates a bug by which we could re-add this data to
                  our list, so using a boolean field 'isRequestUndergoing'
                  and manipulating its value fixes the bug.

                 */
                if (!isRequestUnderGoing) {
                  // get videos list from the snapshot
                  List<Video> newVideosList = getPostsList(
                          jsonData: snapshot.data, type: Globals.VIDEO_TYPE)
                      .cast<Video>();

                  videosList.addAll(newVideosList);
                }

                // show list view of videos
                return _buildVideosListView();
              } else {
                // check if no post is found in the response
                // then show feedback message

                return _buildNoDataFeedback();
              }
              break;
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    // close the open stream
    _streamController.close();
  }

  // retrieve api uri to request for data
  String _getUri() {
    return Globals.YOUTUBE_API_PLAYLIST_URI;
  }

  // construct request parameters for the request
  Map<String, dynamic> _constructParameters() {
    return {
      'key': Globals.YOUTUBE_API_KEY,
      'playlistId': widget.playlistId.toString(),
      'maxResults': Globals.MAX_YOUTUBE_RESULTS.toString(),
      'fields': Globals.YOUTUBE_API_FIELDS_NEEDED,
      'part': Globals.YOUTUBE_API_PARTS_NEEDED,
      'per_page': Globals.NUMBER_OF_POSTS_TO_FETCH_PER_REQUEST.toString(),
      'pageToken': nextVideoPageToken,
    };
  }

  // fetch videos from api
  Future _loadVideos() async {
    try {
      // if no other request is undergoing, or
      // if there are videos but next page token
      // is empty do not load more videos
      if (isRequestUnderGoing) {
        return;
      }
      if (videosList.isNotEmpty && nextVideoPageToken.isEmpty) {
        // close the stream if not closed
        if (!_streamController.isClosed) {
          _streamController.close();
        }
        //animate the list view using its controller
        // for feedback
        return animateScrolling();
      }
      // show the loading indicator
      setState(() {
        isRequestUnderGoing = true;
      });
      // get videos from api server
      Future videos = Parachute.fetchRemoteData(_getUri(),
          parameters: _constructParameters());

      // add the videos to our list
      videos.then((videosData) {
        // add the data to the streamcontroller
        // to show it later with
        // streambuilder
        _streamController.add(videosData);
        // set next page token
        nextVideoPageToken = videosData['nextPageToken'] ?? '';
      }).catchError((error) {
        // add the error to streamcontroller to catch it later
        // with streambuilder
        _streamController.addError(error);
      }).whenComplete(() {
        // hide the loading indicator
        setState(() {
          isRequestUnderGoing = false;
        });
      });
    } catch (e) {
      // show error
      _streamController.addError(e);
    }
  }

  void animateScrolling() {
    double edge = 50.0;
    double offsetFromBottom =
        widget._scrollController.position.maxScrollExtent -
            widget._scrollController.position.pixels;
    if (offsetFromBottom < edge) {
      widget._scrollController.animateTo(
          widget._scrollController.offset - (edge - offsetFromBottom) - (16),
          duration: new Duration(milliseconds: 500),
          curve: Curves.easeOut);
    }
  }

  Container _buildTryAgainButton(BuildContext context) {
    return Container(
      child: FlatButton(
          color: Globals.styles['flatButtonColor'],
          shape: StadiumBorder(),
          onPressed: () {
            setState(() {
              // increment refresh counter
              refreshCounter++;
            });
            // show a message when refresh counter reaches
            // certain limit
            if (refreshCounter >=
                Globals
                    .NUMBER_OF_REFRESH_LIMIT_TO_SHOW_VISIT_WEBSITE_SUGGESTION) {
              // show visit website suggestion
              Scaffold.of(context).hideCurrentSnackBar();
              Scaffold.of(context).showSnackBar(ViewPiece.actionableSnackBar(
                  leftText: "connection error text",
                  rightText: "visit website",
                  rightAction: () => launchURL(Globals.YOUTUBE_CHANNEL_LINK)));
            }
          },
          child: Text(translator.text("try again"))),
    );
  }

  Widget _buildNoDataFeedback() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        MessageIndicator(icon: FontAwesomeIcons.hourglass, message: "no data"),
        FlatButton(
            child: Text(translator.text("try again")),
            color: Globals.styles['flatButtonColor'],
            shape: StadiumBorder(),
            onPressed: () {
              setState(() {});
            })
      ],
    );
  }

  Widget _buildVideosListView() {
    return ListView(
      controller: widget._scrollController,
      children: [
        ListView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return DocumentaryCard(video: videosList[index]);
          },
          itemCount: videosList.length,
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 8.0),
          alignment: Alignment.center,
          child: isRequestUnderGoing
              ? LoadingIndicator(
                  loadingText: "fetching more",
                  refreshCount: refreshCounter,
                )
              : IconButton(
                  icon: Icon(
                    Icons.arrow_downward,
                  ),
                  onPressed: () async {
                    // update videos
                    _loadVideos();
                  },
                ),
        )
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
