/* this is the container for the tabs in home page */

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:molla_multimedia/Environment/all_translations.dart';
import 'package:molla_multimedia/Environment/globals.dart' as Globals;
import 'package:molla_multimedia/Environment/helpers.dart';
import 'package:molla_multimedia/screens/home/documentary_tab.dart';
import 'package:molla_multimedia/screens/home/post_tab.dart';
import 'package:molla_multimedia/screens/home/radio_tab.dart';

class HomeContainer extends StatefulWidget {
  final List<String> _topTabCategories = [
    Globals.ZENA_NAME,
    Globals.WEKTAWI_NAME,
    Globals.WIYIYIT_NAME,
    Globals.RADIO_NAME,
    Globals.DOCUMENTARY_NAME
  ];

  VoidCallback callback;

  HomeContainer({this.callback, Key key}) : super(key: key);

  @override
  HomeContainerState createState() {
    return new HomeContainerState(_topTabCategories);
  }
}

class HomeContainerState extends State<HomeContainer>
    with AutomaticKeepAliveClientMixin<HomeContainer> {
  final List _topTabCategories;

  List<String> _choices = ['fb', 'yt', 'sc'];

  HomeContainerState(this._topTabCategories);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      key: widget.key,
      length: _topTabCategories.length,
      child: Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.white,
            title: Text(
              translator.text('main title'),
              style: TextStyle(color: Theme.of(context).primaryColorDark),
            ),
            actions: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 8.0),
                child: IconButton(
                  icon: Icon(FontAwesomeIcons.globe),
                  color: Colors.black,
                  onPressed: () {
                    launchURL(Globals.BASE_URI);
                  },
                ),
              ),
              PopupMenuButton<String>(
                icon: Icon(
                  FontAwesomeIcons.ellipsisV,
                  color: Colors.black54,
                ),
                onSelected: _select,
                itemBuilder: (BuildContext context) {
                  return _choices.map((String choice) {
                    IconData icon;
                    String text;
                    switch (choice) {
                      case 'fb':
                        icon = FontAwesomeIcons.facebook;
                        text = 'facebook';
                        break;
                      case 'yt':
                        icon = FontAwesomeIcons.youtube;
                        text = 'youtube';
                        break;
                      case 'sc':
                        icon = FontAwesomeIcons.soundcloud;
                        text = 'soundcloud';
                        break;
                      default:
                        icon = FontAwesomeIcons.globe;
                        text = "website";
                    }
                    return PopupMenuItem<String>(
                      value: choice,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Expanded(flex: 2, child: Icon(icon)),
                          Expanded(flex: 4, child: Text(translator.text(text)))
                        ],
                      ),
                    );
                  }).toList();
                },
              ),
            ],
            bottom: TabBar(
              indicatorSize: TabBarIndicatorSize.tab,
              isScrollable: true,
              unselectedLabelColor: Globals.styles['unselectedTabLabelColor'],
              labelColor: Globals.styles['selectedTabLabelColor'],
              unselectedLabelStyle: Globals.styles['unselectedTopTabStyle'],
              labelStyle: Globals.styles['selectedTopTabStyle'],
              tabs: _topTabCategories.map((categoryName) {
                return Container(
                  child: Tab(text: translator.text(categoryName)),
                );
              }).toList(),
            )),
        body: TabBarView(
            children: _topTabCategories
                .map((categoryName) => TabConstructor(categoryName))
                .toList()),
      ),
    );
    ;
  }

  void _select(String value) {
    String launchUrl;
    switch (value) {
      case 'fb':
        launchUrl = Globals.FACEBOOK_ACCOUNT_LINK;
        break;
      case 'yt':
        launchUrl = Globals.YOUTUBE_CHANNEL_LINK;
        break;
      case 'sc':
        launchUrl = Globals.SOUNDCLOUD_ACCOUNT_LINK;
        break;
      default:
        launchUrl = Globals.BASE_URI;
    }

    launchURL(launchUrl);
  }

  @override
  bool get wantKeepAlive => true;
}

class TabConstructor extends StatelessWidget {
  final String tabName;

  const TabConstructor(this.tabName);

  @override
  Widget build(BuildContext context) {
    switch (this.tabName) {
      case Globals.ZENA_NAME:
        return NewsTab(
          tabName: this.tabName,
        );
        break;
      case Globals.WEKTAWI_NAME:
        return WektawiTab(tabName: this.tabName);
        break;
      case Globals.WIYIYIT_NAME:
        return WiyiyitTab(
          tabName: this.tabName,
        );
        break;
      case Globals.RADIO_NAME:
        return RadioTab();
        break;
      case Globals.DOCUMENTARY_NAME:
        return DocumentaryTab(
          playlistId: Globals.YOUTUBE_DOCUMENTARY_PLAYLIST_ID,
        );
      default:
        return Container(
          width: 0,
          height: 0,
        );
    }
  }
}

/*
wiyiyit tab
 */

class WiyiyitTab extends StatefulWidget {
  const WiyiyitTab({
    Key key,
    @required this.tabName,
  }) : super(key: key);

  final String tabName;

  @override
  WiyiyitTabState createState() {
    return new WiyiyitTabState();
  }
}

class WiyiyitTabState extends State<WiyiyitTab>
    with AutomaticKeepAliveClientMixin<WiyiyitTab> {
  @override
  Widget build(BuildContext context) {
    // get saved state
    super.build(context);

    return PostTab(
        categoryIds: Globals.HOME_WIYIYIT_TAB_CATEGORIES,
        selectedCategoryName: this.widget.tabName);
  }

  @override
  bool get wantKeepAlive => true;
}

/*
wektawi tab
 */

class WektawiTab extends StatefulWidget {
  const WektawiTab({
    Key key,
    @required this.tabName,
  }) : super(key: key);

  final String tabName;

  @override
  WektawiTabState createState() {
    return new WektawiTabState();
  }
}

class WektawiTabState extends State<WektawiTab>
    with AutomaticKeepAliveClientMixin<WektawiTab> {
  @override
  Widget build(BuildContext context) {
    // get saved state
    super.build(context);

    return PostTab(
        categoryIds: Globals.HOME_CURRENT_AFFAIRS_TAB_CATEGORIES,
        selectedCategoryName: this.widget.tabName);
  }

  @override
  bool get wantKeepAlive => true;
}

/*
news tab
 */

class NewsTab extends StatefulWidget {
  const NewsTab({
    Key key,
    @required this.tabName,
  }) : super(key: key);

  final String tabName;

  @override
  NewsTabState createState() {
    return new NewsTabState();
  }
}

class NewsTabState extends State<NewsTab>
    with AutomaticKeepAliveClientMixin<NewsTab> {
  @override
  Widget build(BuildContext context) {
    // get saved state
    super.build(context);

    return PostTab(
        categoryIds: Globals.HOME_NEWS_TAB_CATEGORIES,
        selectedCategoryName: this.widget.tabName);
  }

  @override
  bool get wantKeepAlive => true;
}
