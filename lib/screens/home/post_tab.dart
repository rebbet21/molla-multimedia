import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:molla_multimedia/Environment/all_translations.dart';
import 'package:molla_multimedia/Environment/globals.dart' as Globals;
import 'package:molla_multimedia/Environment/helpers.dart';
import 'package:molla_multimedia/Environment/utilities.dart';
import 'package:molla_multimedia/Models/Post.dart';
import 'package:molla_multimedia/views/partials/error_indicator.dart';
import 'package:molla_multimedia/views/partials/loading_indicator.dart';
import 'package:molla_multimedia/views/partials/message_indicator.dart';
import 'package:molla_multimedia/views/view_builders/bottom_post_list_builder.dart';
import 'package:molla_multimedia/views/view_builders/top_posts_list_builder.dart';

class PostTab extends StatefulWidget {
  // list of category ids the news is fetched from
  final List<int> categoryIds;
  // currently selected category (tab)
  final String selectedCategoryName;
  // type of the tab, can be
  // news or video, default
  // is news
  final String tabType;
  // controller
  final ScrollController _controller = ScrollController();

  final String playlistId;

  PostTab(
      {Key key,
      this.categoryIds,
      @required this.selectedCategoryName,
      this.playlistId,
      this.tabType = Globals.NEWS_TYPE})
      : super(key: key);
  @override
  _PostTabState createState() => _PostTabState(this.categoryIds);
}

class _PostTabState extends State<PostTab> {
  _PostTabState(this._categoryIds);

  // key for refresh indicator
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  // list of posts appended when scrolled to bottom
  // at first there is none
  List<Post> _morePostsList = [];

  // memoizer for caching function returns
  // so the future builder wont reload
  // on every setstate
  AsyncMemoizer _memoizer = AsyncMemoizer();

  // state to keep track when new fetch request
  // is sent to server
  bool _isPerformingMorePostsRequest = false;

  // category ids to fetch news from

  final List<int> _categoryIds;
  // refresh counter for feedback purposes

  int _refreshCounter = 0;
  // keep the last page request for

  // default is the first
  int _nextNewsPage = 1;
  // for videos, store the next page token

  // from youtube api
  String _nextVideoPageToken = '';

  @override
  void initState() {
    //attach an event to the controller
    widget._controller.addListener(() async {
      // check and fetch news when scroll is exhausted

      if (widget._controller.position.pixels ==
          widget._controller.position.maxScrollExtent) {
        // fetch and update news
        _loadMorePosts();
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: () {
        return Future.delayed(Duration(milliseconds: 1)).then((results) {
          // reset some states
          setState(() {
            _memoizer = AsyncMemoizer();
            _nextNewsPage = 1;
            _nextVideoPageToken = '';
            _refreshCounter = 0;
            _isPerformingMorePostsRequest = false;
            _morePostsList = [];
          });
        });
      },
      child: FutureBuilder(
          future: _getInitialPosts(),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
                return LoadingIndicator(
                  loadingText: "connecting",
                  refreshCount: _refreshCounter,
                );
                break;
              case ConnectionState.active:
                return LoadingIndicator(
                  loadingText: "active",
                  refreshCount: _refreshCounter,
                );
                break;
              case ConnectionState.done:
                if (snapshot.hasError) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ErrorIndicator(errorText: "connection error text"),
                      _buildTryAgainButton(context)
                    ],
                  );
                } else if (snapshot.hasData) {
                  // parse the data from the future object
                  // and create a list of posts using
                  // the data from the snapshot
                  // add list of new posts
                  // to the all news list

                  List<Post> allNewsList = getPostsList(
                      jsonData: snapshot.data, type: widget.tabType);

                  // append to all news
                  allNewsList.addAll(_morePostsList);

                  if (allNewsList.isEmpty) {
                    return _buildNoDataIndicator();
                  }
                  // show the data
                  return _buildScreenListView(allNewsList);
                } else {
                  // check if no post is found in the response
                  // then show feedback message

                  return _buildNoDataIndicator();
                }
                break;
            }
          }),
    );
  }

  Column _buildNoDataIndicator() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        MessageIndicator(icon: FontAwesomeIcons.hourglass, message: "no data"),
        FlatButton(
            child: Text(translator.text("try again")),
            color: Globals.styles['flatButtonColor'],
            shape: StadiumBorder(),
            onPressed: () {
              setState(() {
                // refresh tab with a new memoizer
                // to reload the future in
                // the future builder
                _memoizer = AsyncMemoizer();
              });
            })
      ],
    );
  }

  _getInitialPosts() {
    return _memoizer.runOnce(() async {
      dynamic posts = await Parachute.fetchRemoteData(_getUri(),
          parameters: _constructParameters());
      // set next page token for videos
      if (widget.tabType == Globals.VIDEO_TYPE) {
        _nextVideoPageToken = posts['nextPageToken'] ?? '';
      }
      return posts;
    });
  }

  // construct parameters based on the selected tab type
  Map<String, dynamic> _constructParameters() {
    if (widget.tabType == Globals.NEWS_TYPE) {
      return {
        'categories': this._categoryIds.join(','),
        'per_page': Globals.NUMBER_OF_POSTS_TO_FETCH_PER_REQUEST.toString(),
        'page': _nextNewsPage.toString(),
        '_embed': ''
      };
    } else if (widget.tabType == Globals.VIDEO_TYPE) {
      return {
        'key': Globals.YOUTUBE_API_KEY,
        'playlistId': widget.playlistId.toString(),
        'maxResults': Globals.MAX_YOUTUBE_RESULTS.toString(),
        'fields': Globals.YOUTUBE_API_FIELDS_NEEDED,
        'part': Globals.YOUTUBE_API_PARTS_NEEDED,
        'per_page': Globals.NUMBER_OF_POSTS_TO_FETCH_PER_REQUEST.toString(),
        'pageToken': _nextVideoPageToken
      };
    } else {
      return {};
    }
  }

// get the uri accordingly
  String _getUri() {
    switch (widget.tabType) {
      case Globals.NEWS_TYPE:
        return Globals.NEWS_URI;
      case Globals.VIDEO_TYPE:
        return Globals.YOUTUBE_API_PLAYLIST_URI;
      default:
        return '';
    }
  }

  Future _loadMorePosts() async {
    // fetch new posts and update the state
    // if no other request is ongoing
    if (!_isPerformingMorePostsRequest) {
      // set is performing request to true
      setState(() {
        // set next page accordingly
        _isPerformingMorePostsRequest = true;

        if (widget.tabType == Globals.NEWS_TYPE) {
          _nextNewsPage++;
        }
      });

      //fetch more posts
      dynamic newPosts = await Parachute.fetchRemoteData(_getUri(),
          parameters: _constructParameters());

      // animate when no data is available
      if (widget.tabType == Globals.NEWS_TYPE && newPosts == null) {
        double edge = 50.0;
        double offsetFromBottom = widget._controller.position.maxScrollExtent -
            widget._controller.position.pixels;
        if (offsetFromBottom < edge) {
          widget._controller.animateTo(
              widget._controller.offset - (edge - offsetFromBottom) - (16),
              duration: new Duration(milliseconds: 500),
              curve: Curves.easeOut);
        }
      }

      // update the state
      setState(() {
        // try to parse the json data and convert to Post
        // object before adding to more news
        try {
          _morePostsList
              .addAll(getPostsList(jsonData: newPosts, type: widget.tabType));
        } catch (e) {
          Scaffold.of(context).hideCurrentSnackBar();
          Scaffold.of(context).showSnackBar(ViewPiece.actionableSnackBar(
              leftText: "connection error text",
              rightText: "visit website",
              rightAction: () => launchURL(Globals.BASE_URI)));
        }

        _isPerformingMorePostsRequest = false;
        // set next page accordingly
        if (widget.tabType == Globals.VIDEO_TYPE) {
          _nextVideoPageToken = newPosts['pageToken'] ?? '';
        }
      });
    }
  }

  Container _buildTryAgainButton(BuildContext context) {
    return Container(
      child: FlatButton(
          color: Globals.styles['flatButtonColor'],
          shape: StadiumBorder(),
          onPressed: () {
            setState(() {
              // refresh tab with a new memoizer
              // to reload the future in
              // the future builder
              _memoizer = AsyncMemoizer();
              // increment refresh counter
              _refreshCounter++;
            });
            // show a message when refresh counter reaches
            // certain limit
            if (_refreshCounter >=
                Globals
                    .NUMBER_OF_REFRESH_LIMIT_TO_SHOW_VISIT_WEBSITE_SUGGESTION) {
              // show visit website suggestion
              Scaffold.of(context).hideCurrentSnackBar();
              Scaffold.of(context).showSnackBar(ViewPiece.actionableSnackBar(
                  leftText: "connection error text",
                  rightText: "visit website",
                  rightAction: () => launchURL(Globals.BASE_URI)));
            }
          },
          child: Text(translator.text("try again"))),
    );
  }

  // list view that contains the whole screen
  Widget _buildScreenListView(List<Post> allNewsList) {
    // check if later contents should be revealed
    bool showBottomContents = allNewsList.length > Globals.topNewsCount;

    return ListView(

        // skip the controller for videos tab (with molla)
        // as it interferes with the nested scroll
        // view of the video page
        controller:
            (widget.tabType == Globals.NEWS_TYPE) ? widget._controller : null,
        children: <Widget>[
          // top post, horizontally scrolling
          Container(
            height: 270,
            child: TopPostsListBuilder(
              allPosts: allNewsList.sublist(
                  0,
                  allNewsList.length >= Globals.topNewsCount
                      ? Globals.topNewsCount
                      : allNewsList.length),
            ),
          ),

          // more news text
          showBottomContents
              ? Column(children: <Widget>[
                  Divider(),
                  Container(
                    child: Text(
                      translator.text('more') +
                          ' ' +
                          translator.text(widget.selectedCategoryName),
                      style: Globals.styles['selectedTopTabStyle'],
                    ),
                  ),
                  Divider(),
                ])
              : Container(
                  width: 0,
                  height: 0,
                ),

          // bottom post
          showBottomContents
              ? BottomPostListBuilder(
                  allPosts: allNewsList.sublist(Globals.topNewsCount),
                )
              : Container(
                  width: 0,
                  height: 0,
                ),

          // loading indicator or
          // load more posts
          // arrow
          _isPerformingMorePostsRequest
              ? Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: LoadingIndicator(
                    loadingText: "fetching more",
                  ),
                )
              : showBottomContents
                  ? Container(
                      margin: EdgeInsets.symmetric(vertical: 8.0),
                      child: IconButton(
                        icon: Icon(
                          Icons.arrow_downward,
                        ),
                        onPressed: () async {
                          // update posts
                          // for videos,
                          // only load if next page is set
                          if (widget.tabType == Globals.VIDEO_TYPE) {
                            if (_nextVideoPageToken.isNotEmpty) {
                              _loadMorePosts();
                            } else {
                              Scaffold.of(context).hideCurrentSnackBar();
                              Scaffold.of(context).showSnackBar(
                                  ViewPiece.actionableSnackBar(
                                      leftText:
                                          translator.text("more from youtube"),
                                      rightText: translator.text("app title"),
                                      rightAction: () => launchURL(
                                          Globals.YOUTUBE_CHANNEL_LINK),
                                      actionIcon: FontAwesomeIcons.youtube));
                            }
                          } else {
                            _loadMorePosts();
                          }
                        },
                      ))
                  : Container(
                      width: 0,
                      height: 0,
                    )
        ]);
  }
}
