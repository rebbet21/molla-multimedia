// custom local class provides localization
// based on selected language

class CustomLocalization {
  //supported languages
  static final _supportedLocales = ['en', 'am'];

// translations
  static final _localization = {
    'en': {
      'title': 'Molla Media',
      'news': 'News',
      'wiyiit': 'Wiyiyit',
      'recent news': 'Recent News',
      'radio': 'Radio',
      'documentary': 'Documentary',
      'home': 'Home',
      'with molla': 'With Molla',
      'connect': 'Connect',
      'settings': 'Settings'
    },
    'am': {
      'title': 'ሞላ ሚዲያ',
      'news': 'ዜና',
      'wiyiit': 'ውውይት ',
      'recent news': 'ወቅታዊ ጉዳይ',
      'radio': 'ሬድዮ',
      'documentary': 'ዘጋቢ',
      'home': 'መግቢያ',
      'with molla': 'ከ ሞላ ጋር ',
      'connect': 'ይገናኙ',
      'settings': 'ቅንብሮች'
    }
  };

  // retrieve the currently available locales
  static List get supportedLocales => _supportedLocales;

  // checks whether the locale is supported
  static bool localeSupported(String locale) {
    return _supportedLocales.contains(locale);
  }

  // get the localized translation
  static String translate(String word) {
    var translatedWord = '';
//    var a = SharedPreferencesHelper.getCurrentLocale().then((currentLocale) {
//      return translateNow(currentLocale, word);
//    });
  }

  static String translateNow(String currentLocale, String word) {
    // check if the current locale is valid
    if (!localeSupported(currentLocale)) {
      return '';
    }

    Map allLocaleTranslations = _localization[currentLocale];
    // check if the word is available
    if (allLocaleTranslations.containsKey(word)) {
      return allLocaleTranslations[word];
    } else {
      return '';
    }
  }
}
