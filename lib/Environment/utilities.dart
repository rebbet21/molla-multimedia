/*
utilities library consists of commonly used functionalities in the application
 */
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:molla_multimedia/Environment/all_translations.dart';
import 'package:molla_multimedia/Environment/globals.dart' as Globals;

// shared preference helper class
// helps get and set shared
// preference values like
// locale settings
class SharedPreferencesHelper {}

// parachute class is used for fetching data from url
class Parachute {
  // this function calls for news content from the web api
  static Future fetchRemoteData(String uri,
      {Map<String, dynamic> parameters}) async {
    // parse the uri to add parameters below
    Uri dataUri = Uri.parse(uri);
    // add query parameters
    dataUri = dataUri.replace(queryParameters: parameters);

    // make asynchronous call to the uri
    var data = await http
        .get(dataUri)
        .timeout(Duration(seconds: Globals.REQUEST_TIMEOUT_SECONDS));

    // check if data is fetched successfully
    if (data.statusCode == 200) {
      // parse and decode the retrieved data
      // serializing it to json
      var decodedData = json.decode(data.body);
      if (decodedData.isEmpty) {
        return null;
      }
      return decodedData;
    }
  }
}

// a class to create persistent tab bar
class SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Container(
      padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * .1),
      alignment: Alignment.center,
      color: Colors.white,
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(SliverAppBarDelegate oldDelegate) {
    return false;
  }
}

// view class helps construct commonly used widgets
class ViewPiece {
  // customized snack bar builder
  static SnackBar actionableSnackBar(
      {String leftText,
      String rightText,
      VoidCallback rightAction,
      IconData actionIcon = Icons.language}) {
    return SnackBar(
        backgroundColor: Globals.styles['topCardBackgroundColor'],
        content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                translator.text(leftText),
                style: TextStyle(color: Colors.grey.shade700),
              ),
              FlatButton(
                color: Globals.styles['flatButtonColor'],
                child: Row(
                  children: <Widget>[
                    actionIcon != null
                        ? Icon(
                            actionIcon,
                            color: Colors.black87,
                          )
                        : Container(
                            width: 0,
                            height: 0,
                          ),
                    Text(
                      " " + translator.text(rightText),
                      style: TextStyle(color: Colors.black87),
                    ),
                  ],
                ),
                onPressed: () {
                  // launch mollamedia.com in a browser
                  rightAction();
                },
              )
            ]));
  }

  static Widget actionableIcon(BuildContext context,
      {ColorSwatch color = Colors.redAccent,
      @required IconData icon,
      ShapeBorder shape = const StadiumBorder(),
      VoidCallback callback,
      int size}) {
    return Material(
      shape: shape,
      color: color,
      child: InkWell(
        onTap: callback,
        customBorder: shape,
        child: Padding(
          padding: EdgeInsets.all(4.0),
          child: icon != null
              ? Icon(
                  Icons.play_arrow,
                  color: Colors.white,
                )
              : Container(
                  width: 0,
                  height: 0,
                ),
        ),
      ),
    );
  }

  static Widget buildRawMaterialButton(
      {@required Color innerColor,
      @required Color outerColor,
      double size = 36,
      ShapeBorder shape = const CircleBorder(),
      IconData icon = Icons.play_arrow,
      VoidCallback action}) {
    return Container(
      width: size,
      height: size,
      child: RawMaterialButton(
        onPressed: action,

        child: new Icon(
          icon,
          color: innerColor,
          size: 30,
        ),
        shape: shape,
        elevation: 2.0,
        fillColor: outerColor,
//        padding: EdgeInsets.all(4),
        //,
      ),
    );
  }
}
