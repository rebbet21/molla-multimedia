import 'package:flutter/material.dart';

/*

Global  sets environment preferences and configurations using directly accessible fields
 */

// pagination of how much data to fetch from the server
const int NUMBER_OF_POSTS_TO_FETCH_PER_REQUEST = 6;
// server request timeout
const int REQUEST_TIMEOUT_SECONDS = 45;
// counter to show a visit website suggestion message
// on repeated network failures
const int NUMBER_OF_REFRESH_LIMIT_TO_SHOW_VISIT_WEBSITE_SUGGESTION = 5;

// unique name for the list view builder key
// this key is used to preserve scroll
// location when getting back
// from other screens
const topNewsUniqueNameKey = "HOME_TOP_NEWS_TAB_";
const bottomNewsUniqueNameKey = 'HOME_BOTTOM_NEWS_TAB_';

// hero tag prefixes for
// top and bottom news
// details
const String topImageHeroTagPrefix = 'top_image_';
const String bottomImageHeroTagPrefix = 'bottom_image_';

const String topTextHeroTagPrefix = 'top_text_';
const String bottomTextHeroTagPrefix = 'bottom_text_';

// top news count sets a fixed number
// of news at the top of the tab
const topNewsCount = 3;

// top videos in molla tab
const topYoutubeVideosCount = 3;

// for news details,
// amount of title character length to check
// if the title sits on top or center
const int TITLE_CHAR_LENGTH_LIMIT_FOR_POSITIONING = 10;

// for bottom navigation bar indexes
const int HOME_PAGE_INDEX = 0;
const int WITH_MOLLA_PAGE_INDEX = 1;
const int SETTINGS_PAGE_INDEX = 2;

// data types
const String NEWS_TYPE = 'news';
const String VIDEO_TYPE = 'video';
const String AUDIO_TYPE = 'audio';

// for app global theme
final appTheme = ThemeData(
  backgroundColor: Colors.white,
//  primaryColor: Colors.green.shade500,
//  primaryColorDark: Colors.green.shade900,
  primaryColor: Color(0xFF0277bd),
  primaryColorDark: Color(0xFF004c8c),
//  accentColor: Color(0xFFFDE428),
  accentColor: Color(0xFFd32f2f),
  accentColorBrightness: Brightness.dark,
  primaryColorLight: Color(0xFF58a5f0),

  fontFamily: "NotoSansAmharic",
);

// styles
final styles = {
  'topCardBackgroundColor': Colors.grey.shade50,
  'bottomNavigationTextStyle':
      TextStyle(fontWeight: FontWeight.w600, fontSize: 14),
  'unselectedTopTabStyle': TextStyle(fontWeight: FontWeight.w400, fontSize: 16),
  'selectedTopTabStyle': TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
  'unselectedTabLabelColor': Colors.grey,
  'selectedTabLabelColor': Colors.black87,
  'title': TextStyle(
    fontSize: 16,
    color: Colors.black,
    fontWeight: FontWeight.w300,
  ),
  'videoTitle': TextStyle(
      fontFamily: "Raleway",
      fontSize: 18,
      color: Colors.black26,
      fontWeight: FontWeight.w500),
  'date': TextStyle(fontWeight: FontWeight.w100, fontSize: 13),
  'flatButtonColor': Colors.grey.shade300,
  'alternateFlatButtonColor': Colors.grey.shade800,
};

// Category IDS for categories in the main website
// used to retrieve posts by category
const int ZENA = 31;
const int HASAB = 1;
const int HATETA = 18;
const int METSAHIFT = 24;
const int MISRAK_AFRICA = 27;
const int AND_SEW = 25;
const int ECONOMY = 23;
const int ENGIDA = 20;
const int WIYIYIT = 21;
const int YEHAGER_NEGER = 26;
const int TIBEBAT = 28;
const int FITIH = 22;
const int WEKTAWI = 32;

// category names
const String ZENA_NAME = 'news';
const String HASAB_NAME = 'hasab';
const String HATETA_NAME = 'hateta';
const String METSAHIFT_NAME = 'metsahift';
const String MISRAK_AFRICA_NAME = 'misrak africa';
const String AND_SEW_NAME = 'and sew';
const String ECONOMY_NAME = 'economy';
const String ENGIDA_NAME = 'engida';
const String WIYIYIT_NAME = 'wiyiyit';
const String YEHAGER_NEGER_NAME = 'yehager neger';
const String TIBEBAT_NAME = 'tibebat';
const String FITIH_NAME = 'fitih';
const String WEKTAWI_NAME = 'recent news';

const String RADIO_NAME = 'radio';
const String DOCUMENTARY_NAME = 'documentary';

// category ids each tab in the home screen consists of
const HOME_NEWS_TAB_CATEGORIES = [ZENA];
const HOME_CURRENT_AFFAIRS_TAB_CATEGORIES = [WEKTAWI];
const HOME_WIYIYIT_TAB_CATEGORIES = [
  HASAB,
  HATETA,
  METSAHIFT,
  MISRAK_AFRICA,
  AND_SEW,
  ECONOMY,
  ENGIDA,
  WIYIYIT,
  YEHAGER_NEGER,
  TIBEBAT,
  FITIH
];
const HOME_RADIO_TAB_CATEGORIES = [];
const HOME_DOCUMENTARY_TAB_CATEGORIES = [];

// uri to fetch data from site
const String BASE_URI = 'https://mollamedia.com';
const String NEWS_URI = BASE_URI + '/wp-json/wp/v2/posts';

// social media accounts
const String FACEBOOK_ACCOUNT_LINK =
    'https://www.facebook.com/MollaMultimedia-907251262997893/';

const String YOUTUBE_ACCOUNT = 'https://youtube.com';
const String GOOGLE_PLUS_ACCOUNT = 'https://plus.google.com';
const String TWITTER_ACCOUNT = 'https://twitter.com';

// sharing links
const String FACEBOOK_SHARER_LINK =
    "https://www.facebook.com/sharer/sharer.php?u=";
const String TWITTER_SHARER_LINK = "https://twitter.com/intent/tweet?url=";

const String TELEGRAM_SHARER_LINK =
    "https://telegram.me/share/url?text=Check This Out&url=";

const String PINTEREST_SHARER_LINK =
    "http://pinterest.com/pin/create/button/?url=";
/*
Youtube
 */
// youtube results limit
const int MAX_YOUTUBE_RESULTS = 6;
// type playlist for youtube search
const String YOUTUBE_PLAYLIST = "playlist";
// type channel for youtube search
const String YOUTUBE_CHANNEL = "channel";
// youtube api key
const String YOUTUBE_API_KEY = "AIzaSyDH1famQPc_-dvqnp3IXma7ybr_r6Qvxc8";
// youtube channel id
const String YOUTUBE_CHANNEL_ID = "UC9MKfOR-mGRFafok-kZF18g";
// youtube molla min ale playlist id
const String YOUTUBE_MOLLA_MIN_ALE_PLAYLIST_ID =
    "PLpglU6cznH7Aj8A-cWJdvNHfKTBo4-X8s";

// youtube documentary playlist id
const String YOUTUBE_DOCUMENTARY_PLAYLIST_ID =
    "PLpglU6cznH7BdRQE_-Q9APBwJGjlOkQz7";

// youtube ke molla ga wiyiyit playlist id
const String YOUTUBE_KE_MOLLA_GA_WIYIYIT_PLAYLIST_ID =
    "PLpglU6cznH7A-68zbVoqxPkghB30oO1qC";
// youtube fields needed in the response
const String YOUTUBE_API_FIELDS_NEEDED =
    'nextPageToken,items(contentDetails(videoId,videoPublishedAt),snippet(title,publishedAt,description, thumbnails(high(url))))';
// youtube parts needed in the response
const String YOUTUBE_API_PARTS_NEEDED = 'snippet,contentDetails';
// youtube channel link
const String YOUTUBE_CHANNEL_LINK =
    "https://www.youtube.com/channel/" + YOUTUBE_CHANNEL_ID;
// google api uri for youtube
const String YOUTUBE_API_BASE_URI = 'https://www.googleapis.com/youtube/v3';
// playlist item uri to fetch videos from
/*
Sound Cloud
 */
const int SOUNDCLOUD_MAX_RESULT_LIMIT = 8;
const String YOUTUBE_API_PLAYLIST_URI = YOUTUBE_API_BASE_URI + '/playlistItems';
const String SOUNDCLOUD_USER_ID = '547148193';
const String SOUNDCLOUD_API_URI =
    'https://api.soundcloud.com/users/' + SOUNDCLOUD_USER_ID + "/tracks";
const String SOUNDCLOUD_ACCOUNT_LINK = 'https://soundcloud.com/user-91670379';
const String SOUNDCLOUD_CLIENT_ID = 'NmW1FlPaiL94ueEu7oziOWjYEzZzQDcK';
