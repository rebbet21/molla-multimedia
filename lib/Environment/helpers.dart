import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_youtube/flutter_youtube.dart';
import 'package:molla_multimedia/Environment/all_translations.dart';
import 'package:molla_multimedia/Environment/globals.dart' as Globals;
import 'package:molla_multimedia/Environment/globals.dart';
import 'package:molla_multimedia/Models/Post.dart';
import 'package:molla_multimedia/Models/audio.dart';
import 'package:molla_multimedia/Models/news.dart';
import 'package:molla_multimedia/Models/video.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
/*
  helper library gives an easy interface to system wide functionalities
 */

// set the status bar color
void setOverlayColor(Color statusBarColor, Color navBarColor) {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: statusBarColor, systemNavigationBarColor: navBarColor));
}

// launch a url
launchURL(String url) async {
  // launch a url if it is not empty
  if (url.isNotEmpty) {
    if (await UrlLauncher.canLaunch(url)) {
      await UrlLauncher.launch(url);
    } else {
      print("Could not launch URL $url");
    }
  }
}

// get elements that exist in the first set
// but not in the second
Iterable<Post> getComplementOf(
    Iterable<Post> firstSet, Iterable<Post> secondSet) {
  return firstSet.where((firstSetElement) {
    secondSet
        .every((secondSetElement) => (firstSetElement != secondSetElement));
  });
}

// get a list of posts from json object
List<Post> getPostsList(
    {@required dynamic jsonData, type = Globals.NEWS_TYPE}) {
  // check for null data
  if (jsonData == null) {
    return [];
  }
  switch (type) {
    case Globals.VIDEO_TYPE:
      if (jsonData is Map) {
        return getVideosList(jsonData);
      }
      break;
    case Globals.NEWS_TYPE:
      if (jsonData is List) {
        return getNewsList(jsonData);
      }
      break;
    case Globals.AUDIO_TYPE:
      if (jsonData is List) {
        return getAudiosList(jsonData);
      }
      break;
  }
  return [];
}

List<Post> getAudiosList(List jsonData) {
  return jsonData.map((singleAudio) {
    try {
      // assume to be a post
      return AudioPost.fromJson(singleAudio) as Post;
    } catch (e) {}
  }).toList();
}

// get news list
List<Post> getNewsList(List jsonData) {
  return jsonData.map((singlePost) {
    try {
      // assume a Post object for polymorphism purposes
      return News.fromJson(singlePost) as Post;
    } catch (e) {
      // do something
      //throw new Exception(e);

    }
  }).toList();
}

// get videos list
List<Post> getVideosList(Map<dynamic, dynamic> jsonData) {
  // select items key's value for
  // list of results
  List itemsData = jsonData['items'];
  // check for null
  if (itemsData == null) {
    return [];
  }
  return itemsData.map((singlePost) {
    try {
      // assume a Post object for polymorphism purposes
      return Video.fromJson(singlePost) as Post;
    } catch (e) {
      // do something
      //throw new Exception(e);

    }
  }).toList();
}

// play youtube video
void playYoutubeVideo(BuildContext context, String videoId) {
  FlutterYoutube.playYoutubeVideoById(
    apiKey: YOUTUBE_API_KEY,
    videoId: videoId,
  );

  Scaffold.of(context).hideCurrentSnackBar();
  Scaffold.of(context)
      .showSnackBar(SnackBar(content: Text(translator.text("playing video"))));
}

// resolve category name from category id
String resolveCategoryName(int id) {
  String resolvedCategoryName = '';
  switch (id) {
    case Globals.ZENA:
      resolvedCategoryName = Globals.ZENA_NAME;
      break;
    case Globals.WEKTAWI:
      resolvedCategoryName = Globals.WEKTAWI_NAME;
      break;
    case Globals.HATETA:
      resolvedCategoryName = Globals.HATETA_NAME;
      break;
    case Globals.METSAHIFT:
      resolvedCategoryName = Globals.METSAHIFT_NAME;
      break;
    case Globals.MISRAK_AFRICA:
      resolvedCategoryName = Globals.MISRAK_AFRICA_NAME;
      break;
    case Globals.AND_SEW:
      resolvedCategoryName = Globals.AND_SEW_NAME;
      break;
    case Globals.ECONOMY:
      resolvedCategoryName = Globals.ECONOMY_NAME;
      break;
    case Globals.ENGIDA:
      resolvedCategoryName = Globals.ENGIDA_NAME;
      break;
    case Globals.YEHAGER_NEGER:
      resolvedCategoryName = Globals.YEHAGER_NEGER_NAME;
      break;
    case Globals.TIBEBAT:
      resolvedCategoryName = Globals.TIBEBAT_NAME;
      break;
    case Globals.FITIH:
      resolvedCategoryName = Globals.FITIH_NAME;
      break;
  }
  // return the translated string
  return translator.text(resolvedCategoryName);
}

String resolveLanguageName(langCode) {
  switch (langCode) {
    case 'am':
      return 'አማርኛ';
      break;
    case 'en':
      return 'English';
      break;
  }
  return '';
}

// construct a readable date
String constructReadableDate(DateTime tm) {
//    // extract year, month, and day
//    // information
//    int year = date.year;
//    int month = date.month;
//    int day = date.day;
//
//    // convert to string and concatinate
//    return year.toString() + "-" + month.toString() + "-" + day.toString();
  if (tm == null) {
    return '';
  }

  DateTime today = new DateTime.now();
  Duration oneDay = new Duration(days: 1);
  Duration twoDay = new Duration(days: 2);
  Duration oneWeek = new Duration(days: 7);
  String month;
  switch (tm.month) {
    case 1:
      month = "january";
      break;
    case 2:
      month = "february";
      break;
    case 3:
      month = "march";
      break;
    case 4:
      month = "april";
      break;
    case 5:
      month = "may";
      break;
    case 6:
      month = "june";
      break;
    case 7:
      month = "july";
      break;
    case 8:
      month = "august";
      break;
    case 9:
      month = "september";
      break;
    case 10:
      month = "october";
      break;
    case 11:
      month = "november";
      break;
    case 12:
      month = "december";
      break;
  }

  Duration difference = today.difference(tm);

  if (difference.compareTo(oneDay) < 1) {
    return translator.text("today");
  } else if (difference.compareTo(twoDay) < 1) {
    return translator.text("yesterday");
  } else if (difference.compareTo(oneWeek) < 1) {
    String weekday = "";
    switch (tm.weekday) {
      case 1:
        weekday = "monday";
        break;
      case 2:
        weekday = "tuesday";

        break;
      case 3:
        weekday = "wednesday";
        break;
      case 4:
        weekday = "thursday";
        break;
      case 5:
        weekday = "friday";
        break;
      case 6:
        weekday = "saturday";
        break;
      case 7:
        weekday = "sunday";
        break;
    }

    return translator.text(weekday);
  } else if (tm.year == today.year) {
    return (translator.text(tm.day.toString()) + ' ' + translator.text(month));
  } else {
    return translator.text(month) +
        ' ' +
        translator.text(tm.day.toString()) +
        ', ' +
        translator.text(tm.year.toString());
  }
}
