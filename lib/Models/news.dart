/*

  News consists of a single news data from the server
 */

import 'package:molla_multimedia/Models/Post.dart';

class News extends Post {
  int _categoryId;
  String _excerpt;
  String _author;
  String _link;

  News.fromJson(Map<String, dynamic> newsData) {
    //populate json data and construct
    //news object
    try {
      this.title = newsData['title']['rendered'];
      this.publishedDate = DateTime.parse(newsData['date']);
      this._categoryId = newsData['categories'][0];
      this.description = newsData['content']['rendered'];
      this._excerpt = newsData['excerpt']['rendered'] ?? '';
      this._author = newsData['_embedded']['author'][0]['name'];
      this._link = newsData['link'];

      this.imageUrl =
          newsData['_embedded']['wp:featuredmedia'][0]['source_url'];
    } catch (e) {
      // set fallback image url
      this.imageUrl = '';
    }
  }

  int get categoryId => _categoryId;

  String get author => _author;

  String get excerpt => _excerpt;

  String get link => _link;
}
