/*

class for audio resources from soundcloud and alike
 */

import 'package:molla_multimedia/Models/Post.dart';

class AudioPost extends Post {
  String _streamUrl;
  String _permalinkUrl;
  int _id;

  AudioPost.fromJson(Map<String, dynamic> jsonData) {
    try {
      this._id = jsonData['id'];
      this.title = jsonData['title'];
      // format date for datetime to parse
      this.publishedDate =
          DateTime.parse(jsonData['created_at'].replaceAll(RegExp(r"/"), '-'));
      this.description = jsonData['description'];
      this._streamUrl = jsonData['stream_url'];
      this._permalinkUrl = jsonData['permalink_url'];
      this.imageUrl = jsonData['artwork_url'];
    } catch (e) {}
  }

  String get streamUrl => _streamUrl;
  String get permalinkUrl => _permalinkUrl;
  int get id => _id;

  @override
  bool operator ==(dynamic anotherAudioPost) {
    if (anotherAudioPost is AudioPost) {
      return this._id == anotherAudioPost.id;
    }
    return false;
  }

  @override
  int get hashCode {
    return this.id;
  }
}
