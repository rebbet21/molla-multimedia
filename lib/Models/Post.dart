/* Post abstract class defines common functionalities used in
news and videos classes
*
* */

abstract class Post {
  String title;
  String description;
  DateTime publishedDate;
  String imageUrl;
}
