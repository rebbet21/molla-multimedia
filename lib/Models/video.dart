/*
class to identify a youtube video source
 */

import 'package:molla_multimedia/Models/Post.dart';

class Video extends Post {
  String _id;

  Video.fromJson(Map<String, dynamic> videoData) {
    //populate json data and construct
    //news object
    try {
      this._id = videoData['contentDetails']['videoId'];
      this.title = videoData['snippet']['title'];
      this.publishedDate =
          DateTime.parse(videoData['contentDetails']['videoPublishedAt']);

      this.description = videoData['snippet']['description'];

      this.imageUrl = videoData['snippet']['thumbnails']['high']['url'] ?? '';
    } catch (e) {}
  }

  String get id => _id;
}
